import Foundation

struct Material {
    let name: String
    let description: String
    var density: Decimal
    var xValue: Decimal
    var yValue: Decimal
    var zValue: Decimal
    
    var weightValue: Decimal {
        return xValue*yValue*zValue*density;
    }
}
