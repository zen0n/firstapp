//
//  ContactViewController.swift
//  FirstApp
//
//  Created by zenon on 4.03.19.
//  Copyright © 2019 zenon. All rights reserved.
//

import UIKit

class MaterialListViewController: UITableViewController {
    let materials: [Material] = [
        Material(name: "Aluminium", description: "density 2770 kg/m3",density:2770,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        Material(name: "Iron", description: "density 7870 kg/m3",density:7870,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        Material(name: "Water", description: "density 1000 kg/m3",density:1000,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        Material(name: "Nylon", description: "density 1700 kg/m3",density:1700,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        Material(name: "Brick", description: "density 2150 kg/m3",density:2150,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        Material(name: "Cement", description: "density 2800 kg/m3",density:2800,xValue: 0.0,yValue: 0.0,zValue: 0.0),
        
    ]
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return materials.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let material = materials[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "MaterialCell", for: indexPath) as! MaterialCell
        cell.nameLabel?.text = material.name
        cell.descriptionLabel?.text = material.description
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let detailViewController = segue.destination as? MaterialItemViewController,
            let index = tableView.indexPathForSelectedRow?.row
            else {
                return
        }
        detailViewController.material = materials[index]
    }
}

class MaterialCell: UITableViewCell {
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var descriptionLabel: UILabel?
}
