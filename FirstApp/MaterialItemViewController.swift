//
//  DetailViewControllerTableViewController.swift
//  FirstApp
//
//  Created by zenon on 4.03.19.
//  Copyright © 2019 zenon. All rights reserved.
//

import UIKit

class MaterialItemViewController: UIViewController {
    
    @IBOutlet var nameLabel: UILabel?
    @IBOutlet var xValueTextField: UITextField!
    @IBOutlet var yValueTextField: UITextField!
    @IBOutlet var zValueTextField: UITextField!
    
    var material: Material?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel?.text = material?.name
        xValueTextField.addTarget(self, action: #selector(xValueTextFieldChanged(_:)), for: .editingChanged)
        yValueTextField.addTarget(self, action: #selector(yValueTextFieldChanged(_:)), for: .editingChanged)
        zValueTextField.addTarget(self, action: #selector(zValueTextFieldChanged(_:)), for: .editingChanged)
    }
    
    @objc func xValueTextFieldChanged(_ textField: UITextField){
        material?.xValue = Decimal(string:textField.text!) ?? 0;
    }
    
    @objc func yValueTextFieldChanged(_ textField: UITextField){
        material?.yValue = Decimal(string:textField.text!) ?? 0;
    }
    
    @objc func zValueTextFieldChanged(_ textField: UITextField){
        material?.zValue = Decimal(string:textField.text!) ?? 0;
    }

    // MARK: - Table view data source
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let resultViewController = segue.destination as? ResultViewController,
            let m = material
            else {
                return
            }
        resultViewController.material = m
    }
}
