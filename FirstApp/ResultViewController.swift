//
//  ResultViewController.swift
//  FirstApp
//
//  Created by zenon on 4.03.19.
//  Copyright © 2019 zenon. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController {
    
    @IBOutlet var materialName: UILabel?
    @IBOutlet var xValue: UILabel?
    @IBOutlet var yValue: UILabel?
    @IBOutlet var zValue: UILabel?
    @IBOutlet var weightValue: UILabel?
    
    var material: Material?

    override func viewDidLoad() {
        super.viewDidLoad()
            materialName?.text = material?.name
            xValue?.text = "\(material?.xValue ?? 0)"
            yValue?.text = "\(material?.yValue ?? 0)"
            zValue?.text = "\(material?.zValue ?? 0)"
            weightValue?.text = "\(material?.weightValue ?? 0)"

    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

